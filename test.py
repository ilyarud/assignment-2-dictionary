import unittest
import subprocess

class Test(unittest.TestCase):

    def run_test(self, test_input):
        pipe = subprocess.PIPE
        proc = subprocess.Popen(
            ["./app"], 
            text = True, 
            shell = True, 
            stdin = pipe, 
            stdout = pipe, 
            stderr = pipe
        )
        stdout, stderr = proc.communicate(input = test_input)

        return (stdout.strip(), stderr.strip())


    def test_element(self):
        self.assertEqual(
            self.run_test("first"), ("1st bober", "")
        )

        self.assertEqual(
            self.run_test("fourth"), ("alsknkiuqdibcw", "")
        )


    def test_element_not_found(self):
        self.assertEqual(
            self.run_test("b00b5"), ("Element not found", "")
        )


    def test_length(self):
        self.assertEqual(
            self.run_test("S"*300), ("Invalid input", "")
        )

    
if __name__ == "__main__":
    unittest.main()