


push:
	@echo "Copying files..."

	@scp -r ../assignment-2-dictionary $(hetz):PL/

	@echo "Done!"

%.o: %.asm
	@nasm -f elf64 -o $@ $<

build: main.o dict.o lib.o
	@ld -o app $^

clean:
	@rm *.o app

test: build
	@python3 test.py

.PHONY: push build clean test