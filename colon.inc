%define next 0

%macro colon 2

    %ifidni %1, dq
        %error "1st argument is not string"
    %endif

    %ifidni %2, db
        %error "2nd argument is not a label"
    %endif

    %2:
        dq next
        db %1, 0

    %define next %2
%endmacro