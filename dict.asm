%include "lib.inc"

section .text

global find_word

find_word:
    push r12
    push r13

    mov r12, rdi
    mov r13, rsi

    .loop:
        test r13, r13
        jz .not_found

        mov rdi, r12
		lea rsi, [r13 + 8]
		call string_equals

		test rax, rax
		jnz .return

		mov r13, [r13]
		jmp .loop

    .return:
        mov rax, r13
        jmp .exit
    .not_found:
        xor rax, rax
    .exit:
        pop r12
        pop r13
        ret
