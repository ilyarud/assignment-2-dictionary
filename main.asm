%include "colon.inc"
%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define MAX_LENGTH 256
%define RESERVED 256

%define OK_CODE 0
%define INPUT_ERROR_CODE 1
%define ELEMENT_NOT_FOUND_ERROR_CODE 2

section .rodata
element_not_found_err_message: 
    db "Element not found", 0

input_err_message: 
    db "Invalid input", 0


section .bss
input: 
    resb RESERVED    ; reserve 256 bytes


section .text

global _start

_start:
    mov rdi, input
    mov rsi, MAX_LENGTH
    call read_word

    test rax, rax
	jz .input_err

    mov rdi, input
	mov rsi, next
	call find_word

    test rax, rax
	jz .elem_not_found_err

    lea rdi, [rax + 8 + rdx + 1]
	call print_string
    call print_newline

    mov rdi, OK_CODE
    call exit
    
    .input_err:
        mov rdi, input_err_message
        call print_error
        call print_newline

        mov rdi, INPUT_ERROR_CODE
        call exit

    .elem_not_found_err:
        mov rdi, element_not_found_err_message
        call print_error
        call print_newline

        mov rdi, ELEMENT_NOT_FOUND_ERROR_CODE
        call exit








